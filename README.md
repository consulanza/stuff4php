# Stuff4Php

Esempi per la standardizzazione del codice

## Installazione

- Copiare l'applicazione in una directory vuota
- esegire composer install

## Uso
- eseguire  php -S localhost:8080 -t public/
- aprire il browser alla pagina http://localhost:8080/samples

L'apertura delle pagine di esempio genera una serie di file di log in var/log/samples

## Caratteristiche
 
### I file di log possono essere separati o comuni

Per ogni procedura è possibile indirizzare i log su file privati o file utilizzati anche da altre procedure.
I file di log utilizzano il nome del canale assegnato alla procedura.
 
### Per ogni procedura vengono generati 3 file di log

- <canale>.log per i messaggi informativi
- <canale>_error.log contenente solo i messaggi di errore
- <canale>_debug.log contenente tutti i messaggi

### Possibilità di ignorare specifici livelli di errore

La classe LogAwareController prevede la configurazione dei livelli di errore da considerare o ignorare;
i metodi log<Level>() della classe fungono da wrapper al logger PSR (monolog) e provvedono 
a verificare l'impostazione per il livello corrispondente. Inoltre fornisce i metodi base
per la gestione dei log nei controller.


## Gestione errori

L'esempio File Upload utilizza la gestione errori personalizzata tramite l'estensione della classe PHP
RuntimeException. Viene definita la classe eccezione di base per le operazioni di upload UploadException e da questa
sono derivate le classi specifiche per le diverse eccezioni che possono essere rilevate.

La classe UploadException provvede a formattare il messaggio utilizzando un template ed il messaggio
specifico della sottoclasse, generando una stringa con un formato standardizzato riportando codice,
segnalazione generica e segnalazione specifica, ottenendo una stinga del tipo: `[<codice>] File Upload Error: <descrizione>`
La descrizione è composta dalle sottoclassi che ridefiniscono il costruttore per richiedere
parametri specifici per il tipo di errore, che dovranno essere passati nell'istruzione di throw.

Ad esempio, per l'errore di tipo file non valido, utilizzeremo: 

```php
throw new InvalidFileTypeException('testfile.doc','.png, .txt, .pdf');
```
che produrrà il messaggio: `[11] File upload error: Invalid file type for testfile.doc. Allowed file types are: .png, .txt, .pdf`

La classe è definito come:

```php
class InvalidFileTypeException extends UploadException
{
    const EXCEPTION_INVALID_FILE_TYPE_MSG   = 'Invalid file type for %s. Allowed file types are: %s';
    const EXCEPTION_INVALID_FILE_TYPE       = 11;
    public function __construct(string $filename, string $allowedTypes)
    {
        parent::__construct(sprintf(self::EXCEPTION_INVALID_FILE_TYPE_MSG, $filename, $allowedTypes), self::EXCEPTION_INVALID_FILE_TYPE);
    }
}
```
vediamo come la ridefinizione del costruttore ci permetta di associare all'eccezione i parametri specifici, in questo caso
il nome file ed una stringa contenente l'elenco dei tipi validi. 

### riepilogo e generalizzazione

Per ogni funzionalità complessa viene creata una classe eccezione di base, che funziona come un'interfaccia
standard; per ogni sottofunzionalità viene creata la sottoclasse specifica che presenta l'interfaccia ottimizzata
per quello specifico aspetto (costruttore personalizzato).
Un ulteriore vantaggio è rappresentato dall'ottenere sempre messaggi coerenti e uniformi, non è più necessario 
ripetere le stringhe dei messaggi, ed il programmatore può concentrarsi sul flusso invece di perdere tempo a ripensare
a come comporre e formattare le segnalazioni.

## MQTT

L'esempio per MQTT è costituio da una pagina web che simula una cassa scontrini. Per le prove richiede
l'accesso ad un broker MQTT (per lo sviluppo è stati stato utilizzat Eclipse Mosquitto) e va installata la libreria
php-mqtt/client (composer require php-mqtt/client, già configurata nel composer.json.)
Sarà inoltre necessario un client MQTT per vedere i messaggi inviati dalla pagina web; nell'esempio
utilizziamo il client Mosquitto.

### note importanti

Per le prove è stato utilizzato Eclipse Mosquitto. Il client Javascript si connette utilizzando
websocket quindi la configurazione di mosquitto dovrà probabilmente essere modificata: per farlo
si può creare il file custom.conf (il nome noe è importante) nella directory /etc/mosquitto/conf.d contenente le
indicazioni:

```ini
# permette l'accesso anonimo
allow_anonymous true
# TCP
port 1883
protocol mqtt
# Websockets
listener 9001
protocol websockets
```

Va inoltre prestata attenzione per la porta; la 9001 potrebbe essere utilizzata da Xdebug per PHP.

### Come provare MQTT

- eseguire  php -S localhost:8080 -t public/
- apire una finestra shell ed eseguire: `mosquitto_sub -d -t stuff4php/samples/cassascontrini`
- aprire il browser alla pagina http://localhost:8080/samples/mqtt/cassascontrini
- selezionare e inserire i dati nella form e inviare

Nella finestra shell verrà visualizzato il messaggio inviato dal controller CassaScontrino.
La classe MqttConfig può essere modificata per impostare i parametri utilizzati per la connessione 
e l'invio alla coda.
Se vengono aperte più finestre shell e in tutte lanciato il comando `mosquitto_sub -d -t stuff4php/samples/cassascontrini`,
i messaggi inviati dalla cassa verrnno visualizzate in tutte le finestre.
Si può anche inviare messaggi da una finestra shell tramite: `mosquitto_pub -d -t stuff4php/samples/cassascontrini -m "Hello world!"`
o aprire più istanze della finestra cassa, tutti i messaggi inviati arriveranno a tutti i client in ascolto.
`
La classe MqttControllerBase definisce le funzionalità che saranno disponibili ai controller
e che potranno essere sovrascritte per creare classi più specializzate.

### riepilogo

Tramite MQTT è possibile costruire sistemi che comunicano attraverso code messaggi, amentando la scalabilità e la possibilità
di rendere asincroni diversi processi.

I componenti di base per MQTT sono:
- broker: server che mantiene le code
- publisher: un client che invia messaggi nad una coda
- subscriber: un client che rimanein osservazione di una coda

inoltre
- qualsiasi client puà essere publisher, subscriber od entrambi
- qualsiasi client può interagire con più code (eventualmente anche con diversi broker)

Un semplice esempio potrebbe essere per un'attività di vendita:

- la cassa invia i dati dello scontrino alla coda `preparazione`. 
- il servizio `preparazione` recupera la merce dal magazzino, prepara il collo e invia un messaggio alla coda `spedizione`
- il servizio `spedizione` preleva il collo e lo spedisce consegna al cliente o al corriere quindi invia un messaggio alla coda `servizio vendite` per la ciusura dell'ordine.

Durante tutti questi passaggi possono essere ovviamente effettuate ulteriori operazioni e lo storico dei messaggi riporta il log delle operazioni effettuate.
Questa architettura, a prima vista non pare differire molto da tecniche già utilizzate come l'implementazione delle code su DB ma
offre notevoli vantaggi per elasticità ed espandibilità:
- livello di astrazione per la gestione dei messaggi: l'applicazione non deve preoccuparsi di gestire tabelle DB e processi per l'implementazione delle code ma
si limita a scambiare messaggi tramite un broker esterno
- il broker può risiedere su un sistema separato, sollevando dalla gestione delle code il sistema dell'applicazione
- la stessa applicazione potrebbe essere suddivisa su più server: installandola su un altro server ed abilitando sul primo solamente l'invio dei messaggi di ordine dal frontend e il secondo per ricezione e trattamento degli ordini.
- gestione logistica centralizzata: i punti vendita inviano i messaggi ad una coda letta dal sistema centrale che in questo modo
può mantenere aggiornate le giacenze in real time e a sua volta aggiornare tramite coda i punti vendita invece di affidarsi a farraginosi meccanismi di
scambio file e macchinoso calcoli che comunque non rispecchieranno mai la situazione precisa. Un punto vendita può accodare un messaggio di ricerca prodotto
per vedere se sia disponibile in qualche altro punto, mentre controlla la coda di ricerca prodotti per segnalare ad altri punti vendita che
ha disponibilità di altri richiesti dai colleghi.
- utilizzando i messagi è possibile creare applicazioni il cui flusso sia gestito tramite eventi e che facilitano l'integrazione di
delle più svariate entità e dispositivi. L'elasticità del sistema permette di aggiungere in qualsiasi momento nuove funzionalità attivate da un evento, senza la necessità
di modificare il codice esistente ma semplicemente aggiungendo un client specifico che osserva la coda ed effettua l'elaborazione richiesta quando arriva il messaggio di suo interesse.
- il monitoraggio si basa sull'osservazione dei messaggi invece di richiedere il termine dell'elaborazione e la lettura del DB;
- l'integrazione fra applicazioni non richiede più la generazione di file da esportare e/o importare ma ha il doppio vantaggio di semplificare la comunicazione
e permettere gli aggiornamenti in tempo reale; non più preoccupazioni per configurare accessi FTP, verificare l'integrità ed i formati, escludere doppioni,
schedulare esportazioni ed importazioni, verificare gli esiti
- le code messaggi offrono per propria natura un ottimo motore per implementare automi a stati finiti.

