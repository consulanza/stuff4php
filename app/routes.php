<?php
$router->add('/',function(){
   $controller = new App\Http\Controllers\HomeController();
   echo $controller->show();
});

$router->add('/hello',function() use ($router){
    echo $router->view_display('hello');
});

$router->add('/samples',function(){
    $controller = new App\Http\Controllers\Samples\Index();
    echo $controller->show();
});

$router->add('/samples/base_logging',function(){
    $controller = new App\Http\Controllers\Samples\BaseLogging();
    echo $controller->show();
});

$router->add('/samples/upload_error_handling',function(){
    $controller = new App\Http\Controllers\Samples\UploadErrorHandling();
    echo $controller->show();
});

$router->add('/samples/mqtt/cassascontrini',function(){
    $controller = new App\Http\Controllers\Mqtt\CassaScontrini();
    echo $controller->show();
});

$router->add('/samples/mqtt/emissionecontrino',function(){
    $controller = new App\Http\Controllers\Mqtt\CassaScontrini();
    echo $controller->emissioneScontrino();
});

$router->add('/samples/mqtt/monitorcassa',function() use ($router){
    $controller = new App\Http\Controllers\Mqtt\CassaScontrini();
    echo $controller->showMonitor();
});

$router->add('/samples/mqtt/cassaajax',function() use ($router){
    $controller = new App\Http\Controllers\Mqtt\CassaAjax();
    echo $controller->show();
});

$router->add('/samples/mqtt/emissionescontrinoajax',function() use ($router){
    $controller = new App\Http\Controllers\Mqtt\CassaAjax();
    echo $controller->emissioneScontrino();
});

$router->add('/samples/upload/uploadfile',function() use ($router){
    $controller = new App\Http\Controllers\Samples\Upload\UploadFile();
    echo $controller->show();
});

$router->add('/samples/upload/douploadfile',function() use ($router){
    $controller = new App\Http\Controllers\Samples\Upload\UploadFile();
    echo $controller->doUploadFile();
});

