<?php
declare(strict_types=1);

namespace App\Http\Controllers\Base;

use App\Helper\Http\Request;
class ControllerBase
{
    private ?Request $request = null;

    protected function getRequest()
    {
        if ($this->request == null) {
            $this->request = new Request();
        }
        return $this->request;
    }

}