<?php
/**
 * Copyright (C) 2022, Consulanza inforrmatica
 * MIT License
 */
declare(strict_types=1);

namespace App\Http\Controllers\Base;

use App\Providers\Router;
use Monolog\Handler\FilterHandler;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface;

abstract class LogAwareController extends ControllerBase
{
    protected string $loggerName        = 'system';
    protected string $logPathTemplate   = '%s/../var/log/%s%s.log';

    private ?LoggerInterface $logger = null;

    /**
     * @var array|\bool[][]
     *
     * Log Levels configuration
     */
    private array $logLevels = [
        Logger::ALERT       => ['enabled' => true],
        Logger::CRITICAL    => ['enabled' => true],
        Logger::DEBUG       => ['enabled' => true],
        Logger::EMERGENCY   => ['enabled' => true],
        Logger::ERROR       => ['enabled' => true],
        Logger::INFO        => ['enabled' => true],
        Logger::NOTICE      => ['enabled' => true],
        Logger::WARNING     => ['enabled' => true]
    ];

    /**
     *
     */
    public function __construct()
    {
        $this->setupHandlers();
    }
    /**
     * @return Logger
     */
    protected function getLogger(): Logger
    {
        if ($this->logger == null) {
            $this->logger = new Logger($this->loggerName);
        }
        return $this->logger;
    }

    /**
     * @param string $suffix
     * @return string
     */
    protected function getLogPath(string $suffix=''): string
    {
        return sprintf($this->logPathTemplate, BASEPATH, $this->loggerName, $suffix);
    }

    /**
     * @return void
     */
    protected function setupHandlers(): void
    {
        $logPath = $this->getLogPath('_debug'); //..._debug.log
        $debugStremHandler = new StreamHandler($logPath, Logger::DEBUG);

        $logPath = $this->getLogPath('_error'); // ..._error.log
        $errorStremHandler = new StreamHandler($logPath, Logger::ERROR);

        $logPath = $this->getLogPath(); // ....log
        $infoStremHandler = new StreamHandler($logPath, Logger::INFO, );
        // customize default log error levels
        $infoFilterHandler = new FilterHandler($infoStremHandler,Logger::DEBUG,Logger::WARNING);

        $logger = $this->getLogger();
        $logger->pushHandler($debugStremHandler);
        $logger->pushHandler($errorStremHandler);
        $logger->pushHandler($infoFilterHandler);
    }

    private function getLevelEnabled(int $logLevel): bool
    {
        return $this->logLevels[$logLevel]['enabled'];
    }

    /**
     * @return bool
     */
    protected function alertEnabled(): bool
    {
        return $this->getLevelEnabled(Logger::ALERT);
    }

    /**
     * @return bool
     */
    protected function criticalEnabled(): bool
    {
        return $this->getLevelEnabled(Logger::CRITICAL);
    }

    /**
     * @return bool
     */
    protected function debugEnabled(): bool
    {
        return $this->getLevelEnabled(Logger::DEBUG);
    }
    /**
     * @return bool
     */
    protected function emergencyEnabled(): bool
    {
        return $this->getLevelEnabled(Logger::EMERGENCY);
    }
    /**
     * @return bool
     */
    protected function errorEnabled(): bool
    {
        return $this->getLevelEnabled(Logger::ERROR);
    }
    /**
     * @return bool
     */
    protected function infoEnabled(): bool
    {
        return $this->getLevelEnabled(Logger::INFO);
    }
    /**
     * @return bool
     */
    protected function noticeEnabled(): bool
    {
        return $this->getLevelEnabled(Logger::NOTICE);
    }
    /**
     * @return bool
     */
    protected function warningEnabled(): bool
    {
        return $this->getLevelEnabled(Logger::WARNING);
    }

    /**
     * @param $message
     * @param array $context
     * @return void
     */
    protected function logAlert($message, array $context = []): void
    {
        if ($this->alertEnabled()) {
            $this->getLogger()->alert($message, $context);
        }
    }

    /**
     * @param $message
     * @param array $context
     * @return void
     */
    protected function logCritical($message, array $context = []): void
    {
        if ($this->criticalEnabled()) {
            $this->getLogger()->critical($message, $context);
        }
    }

    /**
     * @param $message
     * @param array $context
     * @return void
     */
    protected function logDebug($message, array $context = []): void
    {
        if ($this->debugEnabled()) {
            $this->getLogger()->debug($message, $context);
        }
    }

    /**
     * @param $message
     * @param array $context
     * @return void
     */
    protected function logEmergency($message, array $context = []): void
    {
        if ($this->emergencyEnabled()) {
            $this->getLogger()->emergency($message, $context);
        }
    }

    /**
     * @param $message
     * @param array $context
     * @return void
     */
    protected function logError($message, array $context = []): void
    {
        if ($this->errorEnabled()) {
            $this->getLogger()->error($message, $context);
        }
    }

    /**
     * @param $message
     * @param array $context
     * @return void
     */
    protected function logInfo($message, array $context = []): void
    {
        if ($this->infoEnabled()) {
            $this->getLogger()->info($message, $context);
        }
    }

    /**
     * @param $message
     * @param array $context
     * @return void
     */
    protected function logNotice($message, array $context = []): void
    {
        if ($this->noticeEnabled()) {
            $this->getLogger()->notice($message, $context);
        }
    }

    /**
     * @param $message
     * @param array $context
     * @return void
     */
    protected function logWarning($message, array $context = []): void
    {
        if ($this->warningEnabled()) {
            $this->getLogger()->warning($message, $context);
        }
    }


}