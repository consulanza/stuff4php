<?php

namespace App\Http\Controllers\Mqtt;

class MqttConfig
{
    
    private const MQTT_BROKER_HOST = '127.0.0.1';
    private const MQTT_BROKER_PORT = 1883;
    private const MQTT_BROKER_TLS_PORT = 8883;
    
    private const TLS_SERVER_CA_FILE = '';
    private const TLS_CLIENT_CERTIFICATE_FILE = '';
    private const TLS_CLIENT_CERTIFICATE_KEY_FILE = '';
    private const TLS_CLIENT_CERTIFICATE_KEY_PASSPHRASE = null;
    
    private const AUTHORIZATION_USERNAME = '';
    private const AUTHORIZATION_PASSWORD = '';
    private const TOPIC_CASSASCONTRINI = 'stuff4php/samples/cassascontrini';
    
    public static function getBrokerHost(): string
    {
        return self::MQTT_BROKER_HOST;
    }
    
    public static function getBrokerPort(): int
    {
        return self::MQTT_BROKER_PORT;
    }
    
    public static function getBrokerTlsPort(): int
    {
        return self::MQTT_BROKER_TLS_PORT;
    }
    
    public static function getTlsServerCAFile(): string
    {
        return self::TLS_SERVER_CA_FILE;
    }

    public static function getTlsClientCertificateFile(): string
    {
        return self::TLS_CLIENT_CERTIFICATE_FILE;
    }

    public static function getTlsClientCertificateKeyFile(): string
    {
        return self::TLS_CLIENT_CERTIFICATE_KEY_FILE;
    }

    public static function getTlsClientCertificateKeyPassphrase()
    {
        return self::TLS_CLIENT_CERTIFICATE_KEY_PASSPHRASE;
    }

    public static function getAuthorizationUsername(): string
    {
        return self::AUTHORIZATION_USERNAME;
    }

    public static function getAuthorizationPassword(): string
    {
        return self::AUTHORIZATION_PASSWORD;
    }

    public static function getTopicCassaScontrini(): string
    {
        return self::TOPIC_CASSASCONTRINI;
    }
}