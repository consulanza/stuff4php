<?php
/**
 * Copyright (C) 2022, Consulanza inforrmatica
 * MIT License
 */
declare(strict_types=1);
namespace App\Http\Controllers\Mqtt;

use App\Http\Controllers\Base\LogAwareController;
use PhpMqtt\Client\Exceptions\MqttClientException;
use PhpMqtt\Client\Exceptions\ProtocolNotSupportedException;
use PhpMqtt\Client\MqttClient;

class MqttControllerBase extends LogAwareController
{

    private ?MqttClient $client = null;

    protected function getClientId(): string
    {
        return 'test-publisher';
    }

    protected function getClientProtocol(): string
    {
        return MqttClient::MQTT_3_1;
    }

    protected function getPublisherQoS(): int
    {
        return MqttClient::QOS_AT_MOST_ONCE;
    }

    protected function getTopic(): string
    {
        return MqttConfig::getTopicCassaScontrini();
    }

    protected function getClient(array $params=[]): MqttClient
    {

        $host = $params['host'] ?? MqttConfig::getBrokerHost();
        $port = $params['port'] ?? MqttConfig::getBrokerPort();

        try {
            if ($this->client == null) {
                $this->client = new MqttClient(
                    $host,
                    intval($port),
                    $this->getClientId(),
                    $this->getClientProtocol(),
                 null,
                    $this->getLogger());
            }
        } catch (ProtocolNotSupportedException $e) {
            $this->getLogger()->error(sprintf('Protocol not supported: %s. An exception occurred.',
            $this->getClientProtocol()), ['exception' => $e]);
            throw new MqttClientException('Impossibile istanziare il client');
        } catch (\Exception $e) {
            $this->getLogger()->error(sprintf('Errore imprevisto in getclient(): %s.',$e->getMessage()));
        }
        return $this->client;
    }

    protected function publishMessage(string $message, ?string $topic=null, array $params=[])
    {
        $topic = $topic ?? $this->getTopic();
        try {
            $client = $this->getClient($params);
            $client->connect(null, true);
            $client->publish($topic, $message, $this->getPublisherQoS());
            $client->disconnect();
        } catch (MqttClientException $e) {
            // MqttClientException is the base exception of all exceptions in the library. Catching it will catch all MQTT related exceptions.
            $this->getLogger()->error(
                sprintf('Publishing a message using QoS %s failed. An exception occurred.',
                    $this->getPublisherQoS()),
                ['exception' => $e]);
        }
    }

}