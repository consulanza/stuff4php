<?php
/**
 * Copyright (C) 2022,2023 Consulanza Informatica
 * MIT License
 */
declare(strict_types=1);

namespace App\Http\Controllers\Mqtt;

use App\Providers\Router;

class CassaAjax extends MqttControllerBase
{
    const FORMFIELD_HOST        = 'host';
    const FORMFIELD_TCP_PORT    = 'tcp_port';
    const FORMFIELD_TOPIC       = 'topic';
    const FORMFIELD_WS_PORT     = 'ws_port';
    const FORMFIELD_CASSA       = 'cassa';
    const FORMFIELD_ARTICOLO    = 'articolo';
    const FORMFIELD_IMPORTO     = 'importo';
    const FORMFIELD_QUANTITA    = 'quantita';
    const CODA_VENDITE = 'stuff4php/samples/cassascontrini';

    public function show()
    {
        $router = new Router();
        return $router->view_render('samples/mqtt/cassaajax', $this->getHeadVars());
    }

    public function emissioneScontrino()
    {
          $request = $this->getRequest();
        if ($request->isSubmit()) {
            $host               = $request->getPostValue(self::FORMFIELD_HOST, 'localhost');
            $port               = $request->getPostValue(self::FORMFIELD_TCP_PORT, '1883');
            $topic              = $request->getPostValue(self::FORMFIELD_TOPIC, self::CODA_VENDITE);
            $cassa              = $request->getPostValue(self::FORMFIELD_CASSA, 'sconosciuta');
            $articolo           = $request->getPostValue(self::FORMFIELD_ARTICOLO,'generico');
            $importo            = $request->getPostValue(self::FORMFIELD_IMPORTO, '10');
            $quantita           = $request->getPostValue(self::FORMFIELD_QUANTITA, '1');
            $timestamp          = date('Y-m-d H:i:s');
            $messaggioVendita   = sprintf("[%s] Cassa: %s Art.: %s Importo: %s: Qta: %s",
                $timestamp, $cassa, $articolo, $importo, $quantita);
            $this->publishMessage($messaggioVendita, $topic, ['host'=>$host, 'port'=>$port]);
        }
        return "done";
    }
    /**
     * @return array
     */
    protected function getHeadVars(): array
    {
        return [
            'description'   => 'MQTT messaging example',
            'title'         => 'MQTT - Cassa scontrini Ajax con monitor cassa',
        ];
    }

}