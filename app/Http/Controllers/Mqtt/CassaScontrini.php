<?php
/**
 * Copyright (C) 2022,2023 Consulanza Informatica
 * MIT License
 */
declare(strict_types=1);

namespace App\Http\Controllers\Mqtt;

use App\Providers\Router;

class CassaScontrini extends MqttControllerBase
{
    const FORMFIELD_CASSA       = 'cassa';
    const FORMFIELD_ARTICOLO    = 'articolo';
    const FORMFIELD_IMPORTO     = 'importo';
    const FORMFIELD_QUANTITA    = 'quantita';
    const CODA_VENDITE = 'stuff4php/samples/cassascontrini';

    public function show()
    {
        $router = new Router();
        return $router->view_render('samples/mqtt/cassascontrini', $this->getHeadVars());
    }

    public function emissioneScontrino()
    {
        $request = $this->getRequest();
        if ($request->isSubmit()) {
            $cassa              = $request->getPostValue(self::FORMFIELD_CASSA, 'sconosciuta');
            $articolo           = $request->getPostValue(self::FORMFIELD_ARTICOLO,'generico');
            $importo            = $request->getPostValue(self::FORMFIELD_IMPORTO, '10');
            $quantita           = $request->getPostValue(self::FORMFIELD_QUANTITA, '1');
            $messaggioVendita   = sprintf("Cassa: %s Art.: %s Importo: %s: Qta: %s",
                $cassa, $importo, $articolo,$quantita);
            $this->publishMessage($messaggioVendita);
        }
        $router = new Router();
        return $router->view_render('samples/mqtt/cassascontrini', $this->getHeadVars());
    }

    public function showMonitor()
    {
        $router = new Router();
        return $router->view_render('samples/mqtt/monitorcassa', [
            'description'   => 'MQTT messaging example',
            'title'         => 'MQTT - Monitor cassa',
        ]);
    }
    /**
     * @return array
     */
    protected function getHeadVars(): array
    {
        return [
            'description'   => 'MQTT messaging example',
            'title'         => 'MQTT - Cassa scontrini',
        ];
    }

}