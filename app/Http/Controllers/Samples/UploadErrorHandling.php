<?php
namespace App\Http\Controllers\Samples;

namespace App\Http\Controllers\Samples;

use App\Exception\Upload\InvalidFileTypeException;
use App\Exception\Upload\InvalidImageSizeException;
use App\Exception\Upload\PostMaxSizeException;
use App\Exception\Upload\UnableToReadFileException;
use App\Exception\Upload\UnableToWriteFileException;
use App\Exception\Upload\UploadException;
use App\Exception\Upload\UploadMaxFileSizeException;
use App\Http\Controllers\Base\LogAwareController;
use App\Providers\Router;

class UploadErrorHandling extends LogAwareController
{

    const ERROR_EXCEPTION      = 1;
    const ERROR_RUNTIME        = 2;

    const ERROR_TYPE_INVALID_FILE_TYPE      = 11;
    const ERROR_TYPE_INVALID_IMAGE_SIZE     = 12;
    const ERROR_TYPE_POST_MAX_SIZE          = 13;
    const ERROR_TYPE_UNABLE_TO_READ_FILE    = 14;
    const ERROR_TYPE_UNABLE_TO_WRITE_FILE   = 15;
    const ERROR_TYPE_UPLOAD                 = 16;
    const ERROR_TYPE_UPLOAD_MAX_FILE_SIZE   = 17;

    protected string $loggerName        = 'samples/upload.error_handling';
    /**
     * @return false|string
     */
    public function show()
    {
        $this->testErrors();
        $router = new Router();
        return $router->view_display('samples/upload_error_handling');
    }

    protected function testErrors()
    {
        $this->logInfo("Start Upload Errors Sample");
        $this->logDebug("Testing InvalidFileTypeException Error");
        $this->throwError(self::ERROR_TYPE_INVALID_FILE_TYPE);
        $this->logDebug("Testing InvalidImageSizeException Error");
        $this->throwError(self::ERROR_TYPE_INVALID_IMAGE_SIZE);
        $this->logDebug("Testing PostMaxSizeException Error");
        $this->throwError(self::ERROR_TYPE_POST_MAX_SIZE);
        $this->logDebug("Testing UnableToReadFileException Error");
        $this->throwError(self::ERROR_TYPE_UNABLE_TO_READ_FILE);
        $this->logDebug("Testing UnableToWriteFileException Error");
        $this->throwError(self::ERROR_TYPE_UNABLE_TO_WRITE_FILE);
        $this->logDebug("Testing UploadException Error");
        $this->throwError(self::ERROR_TYPE_UPLOAD);
        $this->logDebug("Testing UploadMaxFileSizeException Error");
        $this->throwError(self::ERROR_TYPE_UPLOAD_MAX_FILE_SIZE);
        $this->logDebug("Testing Error");
        $this->throwError(self::ERROR_EXCEPTION);
        $this->logDebug("Testing Error");
        $this->throwError(self::ERROR_RUNTIME);
        $this->logInfo("End Upload Errors Sample");
    }

    protected function throwError(int $errorCode)
    {
        try {
            switch ($errorCode) {
                case self::ERROR_EXCEPTION:
                    throw new \Exception('Test Exception', $errorCode);
                case self::ERROR_RUNTIME:
                    throw new \Exception('Test Runtime Exception', $errorCode);
                case self::ERROR_TYPE_INVALID_FILE_TYPE:
                    throw new InvalidFileTypeException('testfile.doc','.png, .txt, .pdf');
                case self::ERROR_TYPE_INVALID_IMAGE_SIZE:
                    throw new InvalidImageSizeException('10MB');
                case self::ERROR_TYPE_POST_MAX_SIZE:
                    throw new PostMaxSizeException('10MB');
                case self::ERROR_TYPE_UNABLE_TO_READ_FILE:
                    throw new UnableToReadFileException('testfile.txt');
                case self::ERROR_TYPE_UNABLE_TO_WRITE_FILE:
                    throw new UnableToWriteFileException('testfile.txt');
                case self::ERROR_TYPE_UPLOAD:
                    throw new UploadException();
                case self::ERROR_TYPE_UPLOAD_MAX_FILE_SIZE:
                    throw new UploadMaxFileSizeException('5MB');
            }
        }  catch (InvalidFileTypeException $e) {
            $this->logError(sprintf("Intercettato InvalidFileTypeException   codice %s: '%s'. File: %s Linea: %s", $e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine()));
        } catch (UploadMaxFileSizeException $e) {
            $this->logError(sprintf("Intercettato UploadMaxFileSizeException codice %s: '%s'. File: %s Linea: %s", $e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine()));
        } catch (InvalidImageSizeException $e) {
            $this->logError(sprintf("Intercettato InvalidImageSizeException  codice %s: '%s'. File: %s Linea: %s", $e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine()));
        } catch (PostMaxSizeException $e) {
            $this->logError(sprintf("Intercettato PostMaxSizeException       codice %s: '%s'. File: %s Linea: %s", $e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine()));
        } catch (UnableToReadFileException $e) {
            $this->logError(sprintf("Intercettato UnableToReadFileException  codice %s: '%s'. File: %s Linea: %s", $e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine()));
        } catch (UnableToWriteFileException $e) {
            $this->logError(sprintf("Intercettato UnableToWriteFileException codice %s: '%s'. File: %s Linea: %s", $e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine()));
        } catch (UploadException $e) {
            $this->logError(sprintf("Intercettato UploadException            codice %s: '%s'. File: %s Linea: %s", $e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine()));
        } catch (\Exception $e) {
            $this->logError(sprintf("Intercettato Throwable                  codice %s: '%s'. File: %s Linea: %s", $e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine()));
        } catch (\Throwable $e) {
            $this->logError(sprintf("Intercettato Exception                  codice %s: '%s'. File: %s Linea: %s", $e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine()));
        }
    }

}