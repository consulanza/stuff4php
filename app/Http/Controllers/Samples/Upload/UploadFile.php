<?php
/**
 * Copyright (C) 2022,2023 Consulanza Informatica
 * MIT License
 */
declare(strict_types=1);
namespace App\Http\Controllers\Samples\Upload;

use App\Exception\Filesystem\DirectoryAlreadyExitsException;
use App\Exception\Filesystem\FileAlreadyExistsException;
use App\Exception\Filesystem\UploadDirectoryWritePermissionException;
use App\Exception\Upload\PostMaxSizeException;
use App\Exception\Upload\UploadException;
use App\Helper\Filesystem\File as FileManager;
use App\Http\Controllers\Base\LogAwareController;
use App\Providers\Router;
use rawsrc\PhpEcho\PhpEcho;

class UploadFile extends LogAwareController
{
    const UPLOAD_VAR_NAME = 'upload_file';

    private ?string $targetDirectory = null;
    private ?FileManager $fileManager = null;

    public function show()
    {
        $router = new Router();
        return $router->view_render('samples/upload/uploadfile',$this->getHeadVars());
    }

    /**
     * @return array
     */
    protected function getHeadVars(): array
    {
        return [
            'description'   => 'Upload file example',
            'title'         => 'Upload File',
            'post_max_size' => $this->iniGetBytes('post_max_size'),
            'upload_max_filesize' =>  $this->iniGetBytes('upload_max_filesize'),
        ];
    }

    /**
     * @return string
     */
    public function doUploadFile()
    {
        $vars['result'] = 'success';
        $vars['error']  = '';

        $request = $this->getRequest();
        $router = new Router();
        try {
            if ($request->isSubmit()) {
                // filtra subito i casi di errori submit
                $request->checkUploadError(self::UPLOAD_VAR_NAME);
                $targetDir = $this->getTargetDir();
                $fileManager = $this->getFileManager();

                $filename = $request->getUploadFilename(self::UPLOAD_VAR_NAME);
                $targetFile = $targetDir . DIRECTORY_SEPARATOR . $filename;
                if ($fileManager->fileExists($targetFile)) {
                    // file o directory già esistente
                    if ($fileManager->isDir($targetFile)) {
                        throw new DirectoryAlreadyExitsException($targetFile);
                    } else {
                        if (!$this->canOverwriteFile()) {
                            throw new FileAlreadyExistsException($targetFile);
                        } else {
                            $vars['error']  = sprintf('Il file di destinazione %s esisteva già ed è stato sovrascritto',$targetFile);
                        }
                    }
                }
                if (move_uploaded_file($request->getUploadTempFilename(self::UPLOAD_VAR_NAME), $targetFile)) {
                } else {
                    throw new UploadException('Errore non previsto', 0);
                }
            } else {
                $maxPostSize = $this->iniGetBytes('post_max_size');
                if ($this->getContentLength() > $maxPostSize) {
                    throw new PostMaxSizeException($maxPostSize);
                }
            }
        } catch (\RuntimeException $e) {
            $vars['result'] = 'fail';
            $vars['error']  = $e->getMessage();
        }
        return $router->view_render('samples/upload/douploadfile',array_merge($this->getHeadVars(),$vars));
    }

    protected function canOverwriteFile(): bool
    {
        return $this->getRequest()->getCheckboxValue('allow_overwrite');
    }
    protected function getContentLength(): int
    {
        return intval($_SERVER['CONTENT_LENGTH']);
    }

    private function iniGetBytes($val)
    {
        $val = trim(ini_get($val));
        if ($val != '' && !is_numeric($val)) {
            $last = strtolower(substr($val,-1));
            $val = substr($val, 0,-1);
        } else {
            $last = '';
        }
        $val = intval($val);
        switch ($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 'g':
                $val *= 1024;
            // fall through
            case 'm':
                $val *= 1024;
            // fall through
            case 'k':
                $val *= 1024;
            // fall through
        }

        return $val;
    }
    protected function getFileManager()
    {
        if ($this->fileManager == null) {
            $this->fileManager = new FileManager();
        }
        return $this->fileManager;
    }

    /**
     * @return string
     *
     * acquisisce il percorso della directory di upload,
     * provvede a crearla se non esistente e verifica che
     * sia scrivibile
     *
     * @throws UploadDirectoryWritePermissionException
     */
    protected function getTargetDir(): string
    {
        if ($this->targetDirectory == null) {
            $this->targetDirectory = realpath( BASEPATH . '/var/upload');
            $this->getFileManager()->mkDir($this->targetDirectory);
        }
        if (!$this->getFileManager()->isWritable($this->targetDirectory)) {
            throw new UploadDirectoryWritePermissionException($this->targetDirectory);
        }
        return $this->targetDirectory;
    }
}