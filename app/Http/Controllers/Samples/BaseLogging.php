<?php
/**
 * Copyright (C) 2022, Consulanza inforrmatica
 * MIT License
 */

declare(strict_types=1);

namespace App\Http\Controllers\Samples;

use App\Http\Controllers\Base\LogAwareController;
use App\Providers\Router;

/*
 * Monolog supports the logging levels described by RFC 5424.

    DEBUG       (100): Detailed debug information.
    INFO        (200): Interesting events. Examples: User logs in, SQL logs.
    NOTICE      (250): Normal but significant events.
    WARNING     (300): Exceptional occurrences that are not errors. Examples: Use of deprecated APIs, poor use of an API, undesirable things that are not necessarily wrong.
    ERROR       (400): Runtime errors that do not require immediate action but should typically be logged and monitored.
    CRITICAL    (500): Critical conditions. Example: Application component unavailable, unexpected exception.
    ALERT       (550): Action must be taken immediately. Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.
    EMERGENCY   (600): Emergency: system is unusable.

 */

class BaseLogging extends LogAwareController
{
    protected string $loggerName        = 'samples/base_logging';

    /**
     * @return false|string
     */
    public function show()
    {
        $this->testLog();
        $router = new Router();
        return $router->view_display('samples/base_logging');
    }

    /**
     * @return void
     */
    protected function testLog(): void
    {
        $logger = $this->getLogger();
        $logger->debug('Messaggio di debug');
        $logger->info('Messaggio di informazione');
        $logger->notice('Messaggio di notifica');
        $logger->warning('Messaggio di warning');
        $logger->error('Messaggio di errore');
        $logger->critical('Messaggio di errore critico');
        $logger->alert('Messaggio di allerta');
        $logger->emergency('Messaggio di emergenza');
    }
}


