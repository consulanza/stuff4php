<?php
/*
 * Copyright (c) 2022.  ConsuLanza Informatica
 * MIT License
 */
declare(strict_types=1);
namespace App\Http\Controllers\Samples;

use App\Http\Controllers\Base\LogAwareController;
use App\Providers\Router;

class Index extends LogAwareController
{
    public function show()
    {
        $router = new Router();
        return $router->view_display('samples/index');
    }
}