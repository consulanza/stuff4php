<?php
/*
 * Copyright (c) 2022.  ConsuLanza Informatica
 * MIT License
 */

namespace App\Http\Controllers;

use App\Providers\Router;
class HomeController
{
	public function show()
	{
		$router = new Router();
		return $router->view_display('home');
	}
}
