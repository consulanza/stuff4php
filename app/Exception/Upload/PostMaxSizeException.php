<?php
/*
 * Copyright (c) 2022.  ConsuLanza Informatica
 */
declare(strict_types=1);
namespace App\Exception\Upload;

class PostMaxSizeException extends UploadException
{
    const EXCEPTION_POST_MAX_SIZE_MSG   = 'Upload exceeds Post Max Size of %s.';
    const EXCEPTION_POST_MAX_SIZE       = 13;
    public function __construct(int $maxSize)
    {
        parent::__construct(sprintf(self::EXCEPTION_POST_MAX_SIZE_MSG, $maxSize), self::EXCEPTION_POST_MAX_SIZE);
    }
}