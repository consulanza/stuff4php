<?php
/*
 * Copyright (c) 2022.  ConsuLanza Informatica
 */
declare(strict_types=1);
namespace App\Exception\Upload;

class UnableToReadFileException extends UploadException
{
    const EXCEPTION_UNABLE_TO_READ_FILE_MSG   = 'Unable to read file %s.';
    const EXCEPTION_UNABLE_TO_READ_FILE       = 14;
    public function __construct(string $filename)
    {
        parent::__construct(sprintf(self::EXCEPTION_UNABLE_TO_READ_FILE_MSG, $filename), self::EXCEPTION_UNABLE_TO_READ_FILE);
    }
}