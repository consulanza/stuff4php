<?php
/*
 * Copyright (c) 2022.  ConsuLanza Informatica
 */
declare(strict_types=1);
namespace App\Exception\Upload;

class UploadMaxFileSizeException extends UploadException
{
    const EXCEPTION_UPLOAD_MAX_FILE_SIZE_MSG   = 'Upload file max size of %s exceeded.';
    const EXCEPTION_UPLOAD_MAX_FILE_SIZE       = 11;
    public function __construct( int $maxSize)
    {
        parent::__construct(sprintf(self::EXCEPTION_UPLOAD_MAX_FILE_SIZE_MSG, $maxSize), self::EXCEPTION_UPLOAD_MAX_FILE_SIZE);
    }
}