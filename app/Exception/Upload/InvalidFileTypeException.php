<?php
/*
 * Copyright (c) 2022, 2023  ConsuLanza Informatica
 */
declare(strict_types=1);
namespace App\Exception\Upload;

class InvalidFileTypeException extends UploadException
{
    const EXCEPTION_INVALID_FILE_TYPE_MSG   = 'Invalid file type for %s. Allowed file types are: %s';
    const EXCEPTION_INVALID_FILE_TYPE       = 11;
    public function __construct(string $filename, string $allowedTypes)
    {
        parent::__construct(sprintf(self::EXCEPTION_INVALID_FILE_TYPE_MSG, $filename, $allowedTypes), self::EXCEPTION_INVALID_FILE_TYPE);
    }
}