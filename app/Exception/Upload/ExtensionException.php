<?php
/*
 * Copyright (c) 2022, 2023  ConsuLanza Informatica
 */
declare(strict_types=1);
namespace App\Exception\Upload;

class ExtensionException extends UploadException
{
    const EXCEPTION_NO_TMP_DIR_MSG   = 'A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help. ';
    const EXCEPTION_NO_TMP_DIR       = 18;
    public function __construct()
    {
        parent::__construct(self::EXCEPTION_NO_TMP_DIR_MSG, self::EXCEPTION_NO_TMP_DIR);
    }
}