<?php
/*
 * Copyright (c) 2022.  ConsuLanza Informatica
 */
declare(strict_types=1);
namespace App\Exception\Upload;

class InvalidImageSizeException extends UploadException
{
    const EXCEPTION_INVALID_IMAGE_SIZE_MSG   = 'Invalid image size. Requested image size is: %s';
    const EXCEPTION_INVALID_IMAGE_SIZE       = 12;
    public function __construct(string $imageSize)
    {
        parent::__construct(sprintf(self::EXCEPTION_INVALID_IMAGE_SIZE_MSG, $imageSize), self::EXCEPTION_INVALID_IMAGE_SIZE);
    }
}