<?php
/*
 * Copyright (c) 2022.  ConsuLanza Informatica
 */
declare(strict_types=1);
namespace App\Exception\Upload;

class UploadException extends \RuntimeException
{
    const UPLOAD_EXCEPTIONS_BASE_CODE = 1001;

    public function __construct(string $message = '', int $code = 0, \Throwable $parentException = null)
    {
        $code = intval(sprintf('%04d%03d',self::UPLOAD_EXCEPTIONS_BASE_CODE, $code));
        if (empty($message)) {
            parent::__construct(
                sprintf('[%s] File upload error.', $code),
                $code,
                $parentException
            );
        } else {
            $message = sprintf('[%s] File upload error: %s', $code, $message);
            parent::__construct($message, $code, $parentException);
        }
    }



}