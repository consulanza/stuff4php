<?php
/*
 * Copyright (c) 2022, 2023  ConsuLanza Informatica
 */
declare(strict_types=1);
namespace App\Exception\Upload;

class NoTempDirException extends UploadException
{
    const EXCEPTION_NO_TMP_DIR_MSG   = 'Missing temporary directory.';
    const EXCEPTION_NO_TMP_DIR       = 17;
    public function __construct()
    {
        parent::__construct(self::EXCEPTION_NO_TMP_DIR_MSG, self::EXCEPTION_NO_TMP_DIR);
    }
}