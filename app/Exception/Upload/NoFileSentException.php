<?php
/*
 * Copyright (c) 2022, 2023  ConsuLanza Informatica
 */
declare(strict_types=1);
namespace App\Exception\Upload;

class NoFileSentException extends UploadException
{
    const EXCEPTION_NO_FILE_SENT_MSG   = 'No file selected for upload.';
    const EXCEPTION_NO_FILE_SENT       = 16;
    public function __construct()
    {
        parent::__construct(self::EXCEPTION_NO_FILE_SENT_MSG, self::EXCEPTION_NO_FILE_SENT);
    }
}