<?php
declare(strict_types=1);
namespace App\Exception\Filesystem;

class InvalidOperationException extends FilesystemException
{
    const EXCEPTION_INVALID_OPERATION_MSG   = 'Ivalid operation: %s %s';
    const EXCEPTION_INVALID_OPERATION       = 2;
    public function __construct( string $filename, string $reason, int $code=self::EXCEPTION_INVALID_OPERATION)
    {
        parent::__construct(sprintf(self::EXCEPTION_INVALID_OPERATION_MSG, $filename, $reason), $code);
    }

}