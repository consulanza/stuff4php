<?php
/*
 * Copyright (c) 2022.  ConsuLanza Informatica
 */
declare(strict_types=1);
namespace App\Exception\Filesystem;

class FilesystemException extends \RuntimeException
{
    const FILESYSTEM_EXCEPTIONS_BASE_CODE = 1000;

    public function __construct(string $message = '', int $code = 0, \Throwable $parentException = null)
    {
        $code = intval(sprintf('%04d%03d',self::FILESYSTEM_EXCEPTIONS_BASE_CODE, $code));
        if (empty($message)) {
            parent::__construct(
                sprintf('[%s] Filesystem error.', $code),
                $code,
                $parentException
            );
        } else {
            $message = sprintf('[%s] Filesystem error: %s', $code, $message);
            parent::__construct($message, $code, $parentException);
        }
    }

}