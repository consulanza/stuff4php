<?php
declare(strict_types=1);
namespace App\Exception\Filesystem;

class UploadDirectoryWritePermissionException extends FilesystemException
{
    const EXCEPTION_WRITE_PERMISSION_MSG   = 'No write permission to upload directory %s';
    const EXCEPTION_WRITE_PERMISSION       = 12;
    public function __construct( string $filename)
    {
        parent::__construct(sprintf(self::EXCEPTION_WRITE_PERMISSION_MSG, $filename), self::EXCEPTION_WRITE_PERMISSION);
    }
}