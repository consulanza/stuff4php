<?php
declare(strict_types=1);
namespace App\Exception\Filesystem;

class FileAlreadyExistsException extends InvalidOperationException
{
    const EXCEPTION_FILE_ALREADY_EXISTS_MSG   = 'a file with same name already exists';
    const EXCEPTION_FILE_ALREADY_EXISTS       = 10;
    public function __construct( string $filename)
    {
        parent::__construct($filename, self::EXCEPTION_FILE_ALREADY_EXISTS_MSG, self::EXCEPTION_FILE_ALREADY_EXISTS);
    }

}