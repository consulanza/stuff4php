<?php
declare(strict_types=1);
namespace App\Exception\Filesystem;

class DirectoryAlreadyExitsException extends InvalidOperationException
{
    const EXCEPTION_DIR_ALREADY_EXISTS_MSG   = 'a directory with same name already exists';
    const EXCEPTION_DIR_ALREADY_EXISTS       = 11;
    public function __construct( string $filename)
    {
        parent::__construct($filename, self::EXCEPTION_DIR_ALREADY_EXISTS_MSG, self::EXCEPTION_DIR_ALREADY_EXISTS);
    }

}