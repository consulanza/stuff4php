<?php
declare(strict_types=1);
namespace App\Exception\Filesystem;

class IsNotFileException extends InvalidOperationException
{
    const EXCEPTION_IS_NOT_FILE_MSG   = 'is not a file';
    const EXCEPTION_IS_NOT_FILE       = 8;
    public function __construct( string $filename)
    {
        parent::__construct($filename, self::EXCEPTION_IS_NOT_FILE_MSG, self::EXCEPTION_IS_NOT_FILE);
    }

}