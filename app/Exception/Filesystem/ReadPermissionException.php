<?php
declare(strict_types=1);
namespace App\Exception\Filesystem;

class ReadPermissionException extends FilesystemException
{
    const EXCEPTION_READ_PERMISSION_MSG   = 'Read permission denied for file %s';
    const EXCEPTION_READ_PERMISSION       = 5;
    public function __construct( string $filename)
    {
        parent::__construct(sprintf(self::EXCEPTION_READ_PERMISSION_MSG, $filename), self::EXCEPTION_READ_PERMISSION);
    }

}