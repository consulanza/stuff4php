<?php
declare(strict_types=1);
namespace App\Exception\Filesystem;

class IsNotLinkException extends InvalidOperationException
{
    const EXCEPTION_IS_NOT_LINK_MSG   = 'is not a link';
    const EXCEPTION_IS_NOT_LINK       = 9;
    public function __construct( string $filename)
    {
        parent::__construct($filename, self::EXCEPTION_IS_NOT_LINK_MSG, self::EXCEPTION_IS_NOT_LINK);
    }

}