<?php
declare(strict_types=1);
namespace App\Exception\Filesystem;

class IsNotDirException extends InvalidOperationException
{
    const EXCEPTION_IS_NOT_DIR_MSG   = 'is not a directory';
    const EXCEPTION_IS_NOT_DIR       = 7;
    public function __construct( string $filename)
    {
        parent::__construct($filename, self::EXCEPTION_IS_NOT_DIR_MSG, self::EXCEPTION_IS_NOT_DIR);

    }

}