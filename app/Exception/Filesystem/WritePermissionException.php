<?php
/**
 * Copyright (C) 2022,2023 Consulanza Informatica
 * MIT License
 */
declare(strict_types=1);
namespace App\Exception\Filesystem;

class WritePermissionException extends FilesystemException
{
    const EXCEPTION_WRITE_PERMISSION_MSG   = 'Write permission denied for %s';
    const EXCEPTION_WRITE_PERMISSION       = 6;
    public function __construct( string $filename)
    {
        parent::__construct(sprintf(self::EXCEPTION_WRITE_PERMISSION_MSG, $filename), self::EXCEPTION_WRITE_PERMISSION);
    }
}