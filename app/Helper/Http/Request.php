<?php
declare(strict_types=1);
namespace App\Helper\Http;

use App\Exception\Upload\ExtensionException;
use App\Exception\Upload\NoTempDirException;
use App\Exception\Upload\PostMaxSizeException;
use App\Exception\Upload\UnableToWriteFileException;
use App\Exception\Upload\UploadException;
use App\Exception\Upload\UploadMaxFileSizeException;
use App\Exception\Upload\NoFileSentException;

class Request
{
    public const GET   = 'GET';
    public const POST  = 'POST';

    public const SUBMIT = 'submit';

    /**
     * @return string
     */
    public function getRequestMethod(): string
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return bool
     */
    public function isPost(): bool
    {
        return strtoupper($_SERVER['REQUEST_METHOD']) === strtoupper(self::POST);
    }

    public function isGet(): bool
    {
        return strtoupper($_SERVER['REQUEST_METHOD']) === strtoupper(self::GET);
    }

    /**
     * @param string $varName
     * @param $defaultValue
     * @return string
     */
    public function getPostValue(string $varName, string $defaultValue=''): string
    {
        return $_POST[$varName] ?? $defaultValue;
    }

    public function getCheckboxValue(string $varName): bool
    {
        return isset($_POST[$varName]);
    }

    public function isSubmit(): bool
    {
        return $this->isPost() && strtolower($this->getPostValue(self::SUBMIT)) == strtolower(self::SUBMIT);
    }

    public function getUploadFilename(string $varName): string
    {
        return basename($_FILES[$varName]["name"]);
    }
    public function getUploadTempFilename(string $varName): string
    {
        return $_FILES[$varName]["tmp_name"];
    }

    public function getUploadFileSize(string $varName): int
    {
        return intval($_FILES[$varName]["size"] );
    }

    public function getUploadError($varName): array
    {
        return $_FILES[$varName]['error'];
    }

    public function getPostMaxSize():int
    {
        return intval(ini_get('post_max_size'));
    }

    public function getUploadMaxFileSize(): int
    {
        return intval(ini_get('upload_max_filesize'));
    }

    public function checkUploadError($varName): bool
    {
        switch ($_FILES[$varName]['error']) {
            case UPLOAD_ERR_OK:
                return true;
            case UPLOAD_ERR_INI_SIZE:
                throw new UploadMaxFileSizeException($this->getUploadMaxFileSize());
            case UPLOAD_ERR_FORM_SIZE:
                throw new PostMaxSizeException($this->getPostMaxSize());
            case UPLOAD_ERR_NO_FILE:
                throw new NoFileSentException();
            case UPLOAD_ERR_NO_TMP_DIR:
                throw new NoTempDirException();
            case UPLOAD_ERR_CANT_WRITE:
                throw new UnableToWriteFileException($_FILES[$varName]["name"]);
            case UPLOAD_ERR_EXTENSION:
                throw new ExtensionException();
            default:
                throw new UploadException(sprintf("Unhandled PHP upload error code: %s", $_FILES[$varName]['error']));
        }
    }

}