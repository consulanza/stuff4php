<?php
/**
 * Copyright (C) 2022,2023 Consulanza Informatica
 * MIT License
 */
declare(strict_types=1);
namespace App\Helper\Filesystem;

use App\Exception\Filesystem\IsNotDirException;
use App\Exception\Filesystem\WritePermissionException;

class File
{
    private $filename;
    public function __construct(string $filename='noname.tmp')
    {
        $this->filename = $filename;
    }


    public function fileExists(?string $filename=null): bool
    {
        $filename = $filename ?? $this->filename;
        return file_exists($filename);
    }

    public function isDir(?string $filename=null): bool
    {
        $filename = $filename ?? $this->filename;;
        return is_dir($filename);
    }

    public function isFile(?string $filename=null): bool
    {
        $filename = $filename ?? $this->filename;
        return is_file($filename);
    }

    public function isLink(?string $filename=null): bool
    {
        $filename = $filename ?? $this->filename;
        return is_link($filename);
    }
    public function isReadable(?string $filename=null): bool
    {
        $filename = $filename ?? $this->filename;
        return is_readable($filename);
    }

    public function isUploadedFile(?string $filename=null): bool
    {
        $filename = $filename ?? $this->filename;
        return is_uploaded_file($filename);
    }

    public function isWritable(?string $filename=null): bool
    {
        $filename = $filename ?? $this->filename;
        return is_writable($filename);
    }

    public function mkDir(?string $filename=null, int $permissions = 0755, bool $recursive = true): bool
    {
        $filename = $filename ?? $this->filename;
        if ($this->isDir($filename)) {
            return true;
        }
        if ($this->isFile($filename)) {
            throw new IsNotDirException($filename);
        }

        return mkdir($filename, $permissions, $recursive);
    }

    public function rmDir(?string $filename=null): bool
    {
        $filename = $filename ?? $this->filename;
        if (!$this->isDir($filename)) {
            throw new IsNotDirException($filename);
        }
        if (!$this->isWritable($filename)) {
            throw new WritePermissionException($filename);
        }
        return rmdir($filename);
    }

    public function unLink(?string $filename=null)
    {
        $filename = $filename ?? $this->filename;
        if (!$this->isFile($filename) || !$this->isLink($filename)) {
            throw new IsNotFileException($filename);
        }
        if (!$this->isWritable($filename)) {
            throw new WritePermissionException($filename);
        }
        return unlink($filename);
    }


    public function delete(?string $filename=null): bool
    {
        $filename = $filename ?? $this->filename;
        if ($this->isDir($filename)) {
            return $this->rmDir($filename);
        } elseif ($this->isFile($filename) || $this->isLink($filename)) {
            return $this->unLink($filename);
        }
        return false;
    }



}