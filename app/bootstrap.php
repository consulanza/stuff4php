<?php
define('BASEPATH', realpath(__DIR__ . '/../'));

$autoloader = require BASEPATH . '/vendor/autoload.php';

// https://phpocean.com/tutorials/back-end/php-frameworking-routing-autoloading-configuration-part-2/10
require('Providers/Router.php');

$router = new App\Providers\Router();
