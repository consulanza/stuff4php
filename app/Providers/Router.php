<?php
declare(strict_types=1);
namespace App\Providers;

// https://phpocean.com/tutorials/back-end/php-frameworking-routing-autoloading-configuration-part-2/10
use rawsrc\PhpEcho\PhpEcho;
class Router
{
	private array $routes = [];
	private $notFound;

	public function __construct()
	{
		$this->notFound = function ($url) {
			echo "404 - $url was not found!";
		};
	}

	public function add($url, $action)
	{
		$this->routes[$url] = $action;
	}

	public function setNotFound($action)
	{
		$this->notFound = $action;
	}

	public function dispatch()
	{
		if ( ! is_array($this->routes) || empty($this->routes)) {
			return false;
		}
        $requestUri = $this->getRequestUri();
		foreach ($this->routes as $url => $action)
		{
			if ($url == $requestUri) {
				return $action();
			}
		}
		call_user_func_array($this->notFound, [$_SERVER['REQUEST_URI']]);
	}

    /**
     * @return string
     * remove xdebug tag
     */
    protected function getRequestUri(): string
    {
        $requestUri     = $_SERVER['REQUEST_URI'];
        $queryString    = $_SERVER['QUERY_STRING'];
        $debugTag       ='XDEBUG_SESSION_START=';
        if (strpos($queryString, $debugTag) !== false) {
            $requestUri = substr($requestUri, 0, strpos($requestUri,$debugTag) -1);
        }
        return $requestUri;
    }

	public function view_load($view_path, $view_name)
	{
		if (file_exists($view_path . $view_name)) {
			return file_get_contents($view_path . $view_name);
		}
		throw new \Exception("View does not exist: " . $view_path . $view_name);
	}

	public function view_display($view_name)
	{
		$view_path = BASEPATH . '/resources/views/';
		return $this->view_load($view_path, $view_name . '.phtml');
	}

    public function getBasePath(): string
    {

    }

    public function view_render(string $view_name, array $vars=[]): string
    {
        $pageTemplate = sprintf('%s/resources/layout/%s.phtml',BASEPATH, 'main');
        $page = new PhpEcho($pageTemplate, $vars);
        $body = new PhpEcho(sprintf('%s/resources/views/%s.phtml',BASEPATH, $view_name), $vars);
        $page['body'] = $body;
        return $page->__toString();
    }
}
